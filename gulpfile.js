
var
  gulp = require("gulp"),
  del = require('del'),
  clean = require('gulp-clean'),
  sass = require("gulp-sass"),
  autoprefixer = require("gulp-autoprefixer"),
  concat = require('gulp-concat'),
  jsmin = require('gulp-jsmin'),
  minify = require('gulp-minify'),
  sourcemaps = require("gulp-sourcemaps"),
  gutil = require("gulp-util"),
  imagemin = require("gulp-imagemin"),
  debug = require("gulp-debug"),
  browserSync = require("browser-sync").create(),
  rename = require("gulp-rename"),
  gulpCopy = require('gulp-copy'),
  data = require('gulp-data'),
  jsonConcat = require('gulp-json-concat'),
  nunjucksRender = require('gulp-nunjucks-render'),
  uglify = require('gulp-uglify-es').default;


// gulp.task('clean', function(){
//   return del('dist/views/*');
// });

gulp.task('clean', function(cb) {
    del(['./dist/*'], cb);
});

// gulp.task('clean', function () {
//     gulp.src('./dist/*').pipe(clean());
// });


gulp.task("images", function() {
  gulp.src( './src/images/**/*' )
    .pipe(imagemin({
      "verbose": false
    }))
    .pipe(gulp.dest( './dist/images' ));
});


gulp.task('fonts', function(){
  gulp.src( './src/styles/fonts/**/*' )
    .pipe(gulp.dest( './dist/fonts/' ));
});

//// Concatenate, minify and uglify scripts ////
gulp.task('scripts', function(){
    //// This is the list with all the scripts in order /////
		gulp.src([
      //// JQUERY ////
      //'node_modules/jquery/dist/jquery.js',

      //// MAIN JS /////
      './src/scripts/**/*.js'
      ])
		.pipe(jsmin())
    //.pipe(sourcemaps.init())
		.pipe(concat('scripts.js'))
		.pipe(gulp.dest('./dist/scripts/'))
    .pipe(gulp.dest('dist'))
    .pipe(rename('scripts.min.js'))
    .pipe(uglify())
    //.pipe(sourcemaps.write()) // Inline source maps.
    // For external source map file:
    //.pipe(sourcemaps.write("./maps")) // In this case: lib/maps/bundle.min.js.map
    .pipe(gulp.dest( './dist/scripts/' ))
    .pipe(debug({title: "scripts:"}))
    .pipe(browserSync.stream());
});


gulp.task("styles", function() {
  gulp.src( './src/styles/sass/**/*.scss' )
    .pipe(sass({
      outputStyle: 'expanded'
    })
      .on("error", sass.logError))
    .pipe(sourcemaps.init())
    .pipe(autoprefixer({
      browsers: ["last 5 versions"],
      cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(rename('styles.css'))
    .pipe(gulp.dest( './dist/styles/' ))
    .pipe(browserSync.stream());
});

// Copy main index file
gulp.task('copy', function () {
  // gulp.src('./src/views/**/*.*')
  //   .pipe(gulp.dest('./dist/views'))
  gulp.src('./src/index.html')
    .pipe(gulp.dest('dist'));
    gulp.src('./src/view/pages/*')
      .pipe(gulp.dest('dist'));
});


// Concatenate json files
gulp.task('json', function () {
  return gulp.src('./src/views/templates/**/*.json')
    .pipe(jsonConcat('main-data.json',function(data){
      return new Buffer(JSON.stringify(data));
    }))
    .pipe(gulp.dest('./src/views/data'));
});


gulp.task('nunjucks', function() {
  // Gets .html and .nunjucks files in pages
  return gulp.src([
    './src/views/**/*.+(html|nunjucks|njk)',
    '!./src/views/templates/**/*.+(html|nunjucks|njk)'
  ])
  // Adding data to Nnjucks
    .pipe(data(function() {
      return require('./src/views/data/main-data.json')
    }))
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['src/views']
    }))
  // output files in views folder
  .pipe(gulp.dest('dist/views'))
  .pipe(browserSync.stream());
});


// Static server
gulp.task("browser-sync", function() {
  browserSync.init({
    notify: false,
    server: "./",
    open: false,
    logFileChanges: false,
    logConnections: false,
    logLevel: "warn",
    serveStatic: ["./dist"]
  });

  gulp.watch("src/styles/sass/**/*.scss", ["styles"]);
  gulp.watch("src/scripts/*.js", ["scripts"]);
  //gulp.watch("src/styles/fonts/*.+(eot|svg|ttf|otf|woff|woff2)", ["fonts"]);
  gulp.watch("src/styles/scss/*.scss", ['sass']);
  gulp.watch("src/views/templates/**/*.json").on('change', browserSync.reload);
  gulp.watch("src/views/*.html").on('change', browserSync.reload);
  gulp.watch("src/index.html").on('change', browserSync.reload);
  gulp.watch(["src/**/*.+(html|nunjucks|njk)", "src/views/data/*.json"], ["nunjucks"]);
});


gulp.task("watch", function() {
  gulp.watch("src/images/*", ["images"]);
  //gulp.watch("src/styles/fonts/*.+(eot|svg|ttf|otf|woff|woff2)", ["fonts"]);
  gulp.watch("src/styles/sass/**/*.scss", ["styles"]);
  gulp.watch("src/scripts/*.js", ["scripts"]);
  gulp.watch("src/views/*.html", ["copy"]);
  gulp.watch("src/index.html", ["copy"]);
  gulp.watch("src/views/templates/**/*.json", ["json"]);
  gulp.watch(["src/**/*.+(html|nunjucks|njk)", "src/views/data/*.json"], ["nunjucks"]);
});


gulp.task(
  "default",
  ["clean","styles", "images", "scripts", "copy", "nunjucks"]
);
gulp.task(
  "dev",
  ["default","browser-sync","watch"],
  function() {
    gutil.log("-----------------------------------------------");
    gutil.log("      [READY]", "You can now open your browser...");
    gutil.log("-----------------------------------------------");
  }
);
gulp.task(
  "prod",
  ["styles", "images", "scripts", "copy"]
);
